# Hi! Let’s play! - Web application for sport centre reservations and sport centre management

## Gitlab link: 

https://gitlab.upt.ro/victor.cerna/hi-lets-play

## How to run: 

1. Install PostgreSQL and Node.js on your local machine
2. Clone the repository
3. Install the dependencies using: <br />
&nbsp;&nbsp;&nbsp;&nbsp; `yarn` or `yarn install` 
4. Add database credentials in the `app.module.ts` file from the server folder
5. Create `.env` file and fill in the external API’s credentials (this action requires to already have a Google Cloud and Cloudinary account or create new ones; the Google Cloud account needs to enable the Maps JavaScript and Places API):
- NEXTAUTH_URL 
- NEXTAUTH_SECRET 
- NEXT_PUBLIC_PLACES_API_KEY
- NEXT_PUBLIC_MAPS_JAVASCRIPT_API_KEY 
- CLOUDINARY_API_KEY 
- CLOUDINARY_API_SECRET
6. To start the application, type in the terminal: <br />
&nbsp;&nbsp;&nbsp;&nbsp;`yarn start:dev`
